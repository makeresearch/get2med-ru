<div class="how-we-work">
    	<div class="content">
        	<h2>How to use the service</h2>
            <div class="items justify">
            	<div class="item i1">
                	<div class="icon"></div>
                    <div class="text">Search clinic on site</div>
                </div>
                <div class="item i2">
                	<div class="icon"></div>
                    <div class="text">leave the application</div>
                </div>
                <div class="item i3">
                	<div class="icon"></div>
                    <div class="text">Our specialist will contact you
and provide detailed advice</div>
                </div>
                <div class="item i4">
                	<div class="icon"></div>
                    <div class="text">Take decisions, prepare documents a journey</div>
                </div>
            </div>
        </div>
    </div>