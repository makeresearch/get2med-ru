$(document).ready(function() {
    $('.form-block .add-service').click(function(){

        if($('.form-block .add-services').hasClass('open')) {
            $(this).find('.icon').removeClass('ico-minus-white');
            $(this).find('.icon').addClass('ico-plus-white');
            $('.form-block .add-services').slideUp(function(){
                $('.form-block .add-services').removeClass('open');
            });

        } else {
            $(this).find('.icon').addClass('ico-minus-white');
            $(this).find('.icon').removeClass('ico-plus-white');
            $('.form-block .add-services').slideDown(function(){
                $('.form-block .add-services').addClass('open');
            });

        }
    });

    $('.accordion .item .title').click(function(){
        var item = $(this).parent();
        if(item.hasClass('open')) {
            $(this).find('.icon').removeClass('ico-open ico-close');
            $(this).find('.icon').addClass('ico-open');
            item.find('.item-content').slideUp(function(){
                item.removeClass('open');
                item.addClass('close');
            });

        } else {
            $(this).find('.icon').removeClass('ico-open ico-close');
            $(this).find('.icon').addClass('ico-close');
            item.find('.item-content').slideDown(function(){
                item.removeClass('close');
                item.addClass('open');
            });

        }
    });

    $('.header .city span').on('click', function(e){
        $('html,body').stop().animate({ scrollTop: $('.map-wrap').offset().top - 50 }, 1000);
        e.preventDefault();
    });

    $("a.smooth").click(function (){
        var id = $(this).attr("href");
        var pos = $(id).position().top -50;
        $("html, body").animate({scrollTop: pos }, 500);
        return false;
    });

});