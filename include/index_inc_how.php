<div class="how-we-work">
    	<div class="content">
        	<h2>Как пользоваться сервисом</h2>
            <div class="items justify">
            	<div class="item i1">
                	<div class="icon"></div>
                    <div class="text">Найдите клинику на сайте </div>
                </div>
                <div class="item i2">
                	<div class="icon"></div>
                    <div class="text">Оставьте заявку</div>
                </div>
                <div class="item i3">
                	<div class="icon"></div>
                    <div class="text">Наш специалист с вами свяжется
и даст подробную консультацию</div>
                </div>
                <div class="item i4">
                	<div class="icon"></div>
                    <div class="text">Принимаете решение, оформляем документы, отправляетесь в путешествие</div>
                </div>
            </div>
        </div>
    </div>