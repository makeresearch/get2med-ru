<div class="content">
        	<div class="hdr">Портал клиник Кореи</div>
            <div class="text">Получи самую современную медицинскую помощь в ведущих клиниках Кореи. А также
путешествуй по необыкновенной и красивой стране, открывая для себя новые горизонты.</div>
            <a href="/clinics/" class="btn">Перейти к выбору клиник</a>
        </div>
        <div class="number-box">
        	<div class="content">
            	<div class="items justify">
                	<div class="item"><b>22</b> клиники</div>
                    <div class="item"><b>28</b> направлений</div>
                    <div class="item"><b>7</b> городов</div>
                    <div class="item"><b>180</b> профессиональных врачей</div>
                </div>
            </div>
        </div>
    </div>