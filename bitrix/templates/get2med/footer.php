<?
$engTest = substr($_SERVER["REQUEST_URI"], 0, 3);

if ((strpos($engTest, "en")+1) == 2){
    $engTest = "EN";
}
else{
    $engTest = "RU";
}?>
	<?if(
		$APPLICATION->GetCurPage() != '/' AND $APPLICATION->GetCurPage() != '/en/' AND
		$APPLICATION->GetCurPage() != '/clinics/'AND $APPLICATION->GetCurPage() != '/en/clinics/'
	){?>
		</div>
	</div>
	<?}?>

    <div class="footer bg-full-width">
    <div class="content spacer">
        <div class="copy-right">
            <a class="logo" href="<?=SITE_DIR?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="Лого"/></a>
            <?
            if ($engTest == "EN"){
                $tmpPath = "/en/include/";
            }
            else{
                $tmpPath = "/include/";
            }
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "PATH" => $tmpPath . "copyright.php"
                )
            );?>
            <div class="social">
            <?/*$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "PATH" => $tmpPath . "social.php"
                )
            );*/?>
            </div><!--social-->

        </div><!--copy-right-->
        <ul class="site-map">
            <?if ($engTest == "EN"){?>
                <li class="title">SITE MAP</li>
            <?}
            else{?>
                <li class="title">GET2MED</li>
            <?}
        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "bot_menu",
            Array(
                "ROOT_MENU_TYPE" => "bot",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(""),
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "bot",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            )
        );?>

        </ul>

        <? /* <ul class="services">
            <?if ($engTest == "EN"){?>
                <li class="title">DIRECTIONS</li>
            <?}
            else{?>
                <li class="title">НАПРАВЛЕНИЯ</li>
            <?}
        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "bot_menu",
            Array(
                "ROOT_MENU_TYPE" => "directions",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(""),
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "directions",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            )
        );?>
        </ul>
		*/?>
        

        <ul class="contacts">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "PATH" => $tmpPath . "contacts.php"
                    )
            );?>
        </ul>

        <p class="dev">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "PATH" => $tmpPath . "copyright_dev.php"
                    )
            );?>
        </p>

    </div><!--content-->

</div>
<div class="popup" style="display: none;">
	<div class="form-block bg-full-width" id="feedback">
		<div class="content">
			<h3>
				<?if ($engTest == "EN"){?>
					Feedback
				<?}
				else{?>
					Отправьте сообщение и мы свяжемся с Вами в ближайшее время
				<?}?>
			</h3>
			<?$APPLICATION->IncludeComponent(
				"custom:main.feedback",
				"feedback",
				Array(
					"USE_CAPTCHA" => "N",
					"OK_TEXT" => "Спасибо, ваше сообщение принято.",
					"EMAIL_TO" => "",
					"REQUIRED_FIELDS" => array("MAIL", "PHONE"),
					"EVENT_MESSAGE_ID" => array("7"),
					"AJAX_MODE" => 'Y'
				)
			);?>
		</div><!--content-->
	</div><!--form-block-->
</div>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/chosen.jquery.min.js');?>
<script type="text/javascript">
    var config = {
                  '.chosen-select'           : {},
                  '.chosen-select-deselect'  : {allow_single_deselect:true},
                  '.chosen-select-no-single' : {disable_search_threshold:10},
                  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                  '.chosen-select-width'     : {width:"95%"}
                }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</body>
</html>
