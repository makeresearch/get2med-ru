$(document).ready(function() {
    var h_hght = 560;
    var h_mrg = 0;
    $(function(){
        $(window).scroll(function(){
            var top = $(this).scrollTop();
            var elem = $('.page-nav ul');
            if (top+h_mrg < h_hght) {
                elem.removeClass('scrolled');
            } else {
                elem.addClass('scrolled');
            }
        });
    });
    $(".fancybox").fancybox();
	$(".fancybox-feedback").fancybox({
		width: 970
	});

    $(document).on('click', '.form-block .add-service', function(){

        if($('.form-block .add-services').hasClass('open')) {
            $(this).find('.icon').removeClass('ico-minus-white');
            $(this).find('.icon').addClass('ico-plus-white');
            $('.form-block .add-services').slideUp(function(){
                $('.form-block .add-services').removeClass('open');
            });

        } else {
            $(this).find('.icon').addClass('ico-minus-white');
            $(this).find('.icon').removeClass('ico-plus-white');
            $('.form-block .add-services').slideDown(function(){
                $('.form-block .add-services').addClass('open');
            });

        }
    });

    $('.accordion .item .title').click(function(){
        var item = $(this).parent();
	    if(!item.hasClass('disabled')) {
		    if (item.hasClass('open')) {
			    $(this).find('.icon').removeClass('ico-open ico-close');
			    $(this).find('.icon').addClass('ico-open');
			    item.find('.item-content').slideUp(function () {
				    item.removeClass('open');
				    item.addClass('close');
			    });

		    } else {
			    $(this).find('.icon').removeClass('ico-open ico-close');
			    $(this).find('.icon').addClass('ico-close');
			    item.find('.item-content').slideDown(function () {
				    item.removeClass('close');
				    item.addClass('open');
			    });

		    }
	    }
    });

    $('.header .city span').on('click', function(e){
        $('html,body').stop().animate({ scrollTop: $('.map-wrap').offset().top - 50 }, 1000);
        e.preventDefault();
    });

    $("a.smooth").click(function (){
        var id = $(this).attr("href");
        var pos = $(id).position().top -50;
        $("html, body").animate({scrollTop: pos }, 500);
        return false;
    });
    $(".clinic-doctors .view-all").click(function (){
        $(".clinic-doctors .spacer").animate({'max-height': 5000 }, 1000);
        $(this).hide();
    });
    //$('select').selectbox();
    /*$('.header .callback button').on('click', function(e){
        $('html,body').stop().animate({ scrollTop: $('.map-wrap').offset().top - 50 }, 1000);
        e.preventDefault();
    });*/

});

$(document).on('click','.filter-button',function(){
    // if ($(".select-list").val() == null) {
    //     alert('Выберите нужные направления!');
    //     return false;
    // };
    window.location=window.location.pathname+'?directions='+$(".select-list").val();
});

$(document).on('click','.view-number li',function(){
    $(this).addClass('active');
    $.ajax({
        type: "POST",
        url: "/ajax/news_count.php",
        data: {
            count:$(this).val(),
        }
    });
    location.reload();
});

$(document).ready(function(){
    function changeViewport(){
        setTimeout(function(){
            var tmpHeight = $(".rev-text .bx-viewport").attr("style");
            var pos1 = tmpHeight.indexOf("height");
            var oldStyle = tmpHeight.substr(0, pos1);
            tmpHeight = tmpHeight.substr(pos1 + 8);
            tmpHeight = parseInt(tmpHeight);
            tmpHeight += 5;
            tmpHeight = "height: " + tmpHeight + "px;";
            tmpHeight = oldStyle + tmpHeight;
            $(".rev-text .bx-viewport").attr("style", tmpHeight);
        }, 500);
    };
    changeViewport();
});

function declension(num, expressions) {
    var result;
    count = num % 100;
    if (count >= 5 && count <= 20) {
        result = expressions['2'];
    } else {
        count = count % 10;
        if (count == 1) {
            result = expressions['0'];
        } else if (count >= 2 && count <= 4) {
            result = expressions['1'];
        } else {
            result = expressions['2'];
        }
    }
    return result;
}

var clinicword = ['клиникy','клиники','клиник'];
$(document).on('click','.items.justify input[type=checkbox]',function(){
    var arDirections = [];
    $('.items.justify input[type=checkbox]').each(function(){
        if ($(this).prop("checked")) {
            if ($.inArray($(this).val(), arDirections) == -1) {
                arDirections.push($(this).val());
            };
        };
    });
    $('.directions-filter-main').attr('href',$('.directions-filter-main').attr('data-location')+'?directions='+arDirections);
    $.ajax({
        type: "POST",
        url: "/ajax/clinic_direction_filter.php",
        data: {
            directions:arDirections,
        },
        success: function(msg){
            $('.directions-filter-main').text('Показать ('+msg+') '+declension(msg,clinicword));

        }
    });
});
$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: "/ajax/clinic_direction_filter.php",
        success: function(msg){
            $('.directions-filter-main').text('Показать ('+msg+') '+declension(msg,clinicword));
        }
    });
});
$(document).on('click','.items.justify input[type=checkbox]',function(){
    var arDirections = [];
    $('.items.justify input[type=checkbox]').each(function(){
        if ($(this).prop("checked")) {
            if ($.inArray($(this).val(), arDirections) == -1) {
                arDirections.push($(this).val());
            };
        };
    });
    $('.directions-filter-main-en').attr('href',$('.directions-filter-main-en').attr('data-location')+'?directions='+arDirections);
    $.ajax({
        type: "POST",
        url: "/en/ajax/clinic_direction_filter.php",
        data: {
            directions:arDirections,
        },
        success: function(msg){
            $('.directions-filter-main-en').text('Show ('+msg+') '+' clinics');

        }
    });
});
$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: "/en/ajax/clinic_direction_filter.php",
        success: function(msg){
            $('.directions-filter-main-en').text('Show ('+msg+') '+' clinics');
        }
    });

    $('.example b').on('click', function(){
        $('.content .search input[type=text]').val($(this).text());
    });
});

/***********myEdit***********/
$(document).ready(function(){
    var h_hght = 560;
    var h_mrg = 0;
    $(function(){
        $(window).scroll(function(){
            var top = $(this).scrollTop();
            var elem = $('.page-nav .konsButton');
            if (top+h_mrg < h_hght) {
                elem.attr("style", "display: none");
            } else {
                elem.attr("style", "display: inline");
            }
        });
    });
});