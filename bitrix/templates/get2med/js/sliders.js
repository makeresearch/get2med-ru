$(document).ready(function(){

    var slide1_curr_img;
    var slider_img = $('.slider-img');
	if($('.slider-img li').length > 2) {
		slider_img.bxSlider({
			slideWidth: 170,
			mode: 'vertical',
			minSlides: 1,
			maxSlides: 1,
			slideMargin: 0,
			startSlide: 1,
			speed: 350,
			adaptiveWidth: true,
			nextSelector: '#slider-img-next',
			prevSelector: '#slider-img-back',
			nextText: '→',
			prevText: '←',
			auto: false,
			onSliderLoad: function () {
				var slide1_curr_img = slider_img.getCurrentSlide();
				$('.slider-img > li').eq(slide1_curr_img).addClass('active');
			},
			onSlideAfter: function () {
				slide1_curr_img = slider_img.getCurrentSlide();
				slider_text.goToSlide(slide1_curr_img - 1);

				$('.slider-img > li').removeClass('active');
				$('.slider-img > li').eq(slide1_curr_img).addClass('active');
			}
		});
	} else {
		slider_img.css('margin-top', 0);
		slider_img.bxSlider({
			slideWidth: 170,
			mode: 'vertical',
			minSlides: 1,
			maxSlides: 1,
			slideMargin: 0,
			startSlide: 0,
			speed: 350,
			infiniteLoop: false,
			adaptiveWidth: true,
			nextSelector: '#slider-img-next',
			prevSelector: '#slider-img-back',
			nextText: '→',
			prevText: '←',
			auto: false,
			onSliderLoad: function () {
				var slide1_curr_img = slider_img.getCurrentSlide();
				$('.slider-img > li').eq(slide1_curr_img).addClass('active');
			},
			onSlideAfter: function () {
				slide1_curr_img = slider_img.getCurrentSlide();
				slider_text.goToSlide(slide1_curr_img - 1);

				$('.slider-img > li').removeClass('active');
				$('.slider-img > li').eq(slide1_curr_img).addClass('active');
			}
		});
	}

    

    var slider_text = $('.slider-text');
    slider_text.bxSlider({
        slideWidth: 675,
        mode: 'vertical',
        easing: 'easeInBack',
        minSlides: 1,
        maxSlides: 1,
        slideMargin:5,
        startSlide: 0,
        speed:350,
        adaptiveWidth: true,
        nextSelector: '#slider-text-next',
        prevSelector: '#slider-text-back',
        nextText: '→',
        prevText: '←',
        autoDirection: 'next',
        auto: false,
        onSliderLoad: function (){

        },
        onSlideAfter: function (){

        }
    });

    $('.gallery-slider').bxSlider({
        slideWidth:1170,
        auto: false,
        minSlides: 1,
        maxSlides: 1,
	    infiniteLoop: false,
        adaptiveWidth: true,
        nextSelector: '#slider-gallery-next',
        prevSelector: '#slider-gallery-back',
        nextText: '→',
        prevText: '←',
    });
     
});