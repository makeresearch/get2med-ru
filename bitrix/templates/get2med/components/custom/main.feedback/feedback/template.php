<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<form action="<?=POST_FORM_ACTION_URI?>" method="POST" id="feedbackform" name="feedbackform">
<?if(count($arResult["ERROR_MESSAGE"]) > 0)
{
    print '<div class="error">';
	foreach($arResult["ERROR_MESSAGE"] as $v)
		echo $v."<br/>";
    print '</div>';
}
elseif(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?>
    <div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div>
    <?
}

?>
<?=bitrix_sessid_post()?>

    <textarea name="text" placeholder="<?echo getMessage("REV_TEXT")?>" class="box"></textarea>
    <input type="text" name="name" placeholder="<?echo getMessage("REV_NAME")?>"/>
    <input type="text" name="email" placeholder="<?echo getMessage("REV_MAIL")?> *"/>
    <input type="text" name="phone" placeholder="<?echo getMessage("REV_PHONE")?> *"/>

    <div class="form-footer">
        <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
        <button type="submit" name="submit" form="feedbackform" value="submit"><?echo getMessage("REV_SEND_BUTTON")?></button>
        <p class="privacy">
            <?echo getMessage("REV_PR_INFO")?>
        </p>

    </div><!--form-footer-->

</form>

 