<?
$MESS ['REV_TEXT'] = "Messag etext";
$MESS ['REV_NAME'] = "You name";
$MESS ['REV_MAIL'] = "E-mail";
$MESS ['REV_PHONE'] = "Contact phone";
$MESS ['REV_ADDITIONAL_SERVICES'] = "Additional services";
$MESS ['REV_SEND_BUTTON'] = "Send mail";
$MESS ['REV_PR_INFO'] = "The entered information will not be disclosed to third <br/>
            parties and used to send advertising.";
$MESS ['REV_MORE_SERV'] = "Every room in the hospital undergoing treatment in South Korea, we offer you the following additional services: <br/>
Airport shuttle <br/>
Guide city <br/>
Translation services (including during your stay in the clinic) <br/>";
$MESS ['REV_TEXTAREA'] = "";
?>