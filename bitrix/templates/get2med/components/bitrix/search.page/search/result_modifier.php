<?
$counter = 0;
foreach ($arResult["SEARCH"] as $arViewedItem):
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_TOWN", "PROPERTY_USER_RATING", "PROPERTY_OPERATIV", "PROPERTY_PRICE_QUALITY",
     "PROPERTY_DEGREE_OF_SERVICE", "PROPERTY_COST_OF_SERVICES", "PROPERTY_DIRECTIONS_DESCRIPTION", "DETAIL_PAGE_URL");
    $arFilter = Array("ID"=>IntVal($arViewedItem["ITEM_ID"]), "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    
     
    while ($arFld = $res->getNextElement()){
       
      $arResult["SEARCH"]['ITEMS'][$counter] = $arFld->getFields();
      $arResult["SEARCH"]['ITEMS'][$counter]["PROPERTIES"] = $arFld->getProperties();
      
    }
    foreach ($arResult["SEARCH"]['ITEMS'][$counter]['PROPERTIES']['DIRECTIONS_FILTER']['VALUE'] as $value) {
        $direction = CIBlockElement::GetByID($value);
        $direction = $direction->GetNext();
        $arResult["SEARCH"]['ITEMS'][$counter]['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"][] = $direction["NAME"];
    }
    
    $arResult["SEARCH"]["ITEMS"][$counter]["PREVIEW_SRC"] = CFile::GetPath($arResult["SEARCH"]["ITEMS"][$counter]["PREVIEW_PICTURE"]);
    $counter ++;
endforeach;

