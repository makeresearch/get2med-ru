<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<?
$tmp = substr($_SERVER["REQUEST_URI"], 0, 3);
$path = "";
if (strpos($tmp, "/en") === 0){
	$path = "en/";
}
else{
	$path = "";
}
if (empty($arResult["TO_FILTER"])){
	echo "<h2>Не найдено клиник по данному направлению</h2>";
}
else{?>

<script>window.location.replace("/<?echo $path?>clinics/?directions=<?echo $arResult['TO_FILTER']?>");</script>
<?}