<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="top-box">
    <div class="content">
        <h2><?echo GetMessage("TOP_TOP")?></h2>
        <?
        $currItem = 0;
        $dopClass = "";
        foreach ($arResult["ITEMS"] as $arItem):
            $ratingViewed = $arItem["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] + $arItem["PROPERTIES"]["PRICE_QUALITY"]["VALUE"];
            $ratingViewed += $arItem["PROPERTIES"]["OPERATIV"]["VALUE"] + $arItem["PROPERTIES"]["USER_RATING"]["VALUE"];
            $ratingViewed = $ratingViewed/4;
            $ratingViewed = round($ratingViewed, 1);
            $tmpService = CIBlockElement::GetById($arItem["PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"][0]);
            $tmpService = $tmpService->getNext();
            $arItem['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"] = $tmpService["NAME"];
            ?>
            <?if ($currItem == 0):?>
                <div class="box col-left big">
            <?elseif ($currItem == 1):
                $dopClass = " col-left";
                $itemCount = 1;
                ?>
                </div>
                <div class="box col-right group">
            <?elseif ($currItem == 5):
                $itemCount = 2;
            ?>
                </div>
            <?endif?>

                <div class="item<?echo $dopClass?>">
                    <?if($itemCount == 1):?>
                        <?$NewImg = webImage('top-clinics-small', $arItem["PREVIEW_PICTURE"]["SRC"]);?>
                        <img class="222" src="<?=$NewImg["FILE_NAME"]?>" width="<?=$NewImg['WIDTH']?>" height="<?=$NewImg['HEIGHT']?>" alt="" />
                    <?else:?>
                        <?$NewImg = webImage('top-clinics-big', $arItem["PREVIEW_PICTURE"]["SRC"]);?>
                        <img class="111" src="<?=$NewImg["SRC"]?>" width="<?=$NewImg['WIDTH']?>" height="<?=$NewImg['HEIGHT']?>" alt="" />
                    <?endif;?>
                    <div class="info">
                        <div class="sticker"><?echo $currItem+1 . GetMessage("TOP_PLACE")?></div>
                        <p class="item-name"><?echo $arItem["NAME"]?></p>
                        <div class="item-raring">
                             <?$i = 0;
                            for ($i = 0; $i < intVal($ratingViewed); $i ++){?>
                                <i class="icon ico-star-org-slt"></i>
                            <?}
                            for ($j = $i; $j < 5; $j ++){?>
                                <i class="icon ico-star-org"></i>
                            <?}?>
                        </div>
                        <p class="item-service">
                            <i class="icon ico-service-blue"></i>
                            <span class="text"><?echo $arItem['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"]?></span>
                        </p>
                        <p class="item-city">
                            <i class="icon ico-mark-blue"></i>
                            <span class="text"><?echo GetMessage("TOP_CITY") . $arItem["PROPERTIES"]["TOWN"]["VALUE"]?></span>
                        </p>
                        <?if(!empty($arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"])):?>
                            <p class="item-price">
                                <i class="icon ico-price-blue"></i>
                                <span class="text"><?echo GetMessage("TOP_PRICE", Array("#TOP_PRICE" => $arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]))?></span>
                            </p>
                        <?endif;?>
                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="btn"><?echo GetMessage("TOP_DETAILS")?></a>
                        
                    </div>
                </div>
            <?
            $currItem ++;
            if ($dopClass == " col-left"):
                $dopClass = " col-right";
            elseif ($dopClass == " col-right"):
                echo "<div class=\"clear\"></div>";
                $dopClass = " col-left";
            else:
                $dopClass = " col-left";
            endif;
        endforeach;
        $tmp = substr($_SERVER["REQUEST_URI"], 0, 3);
        if ((strpos($tmp, "en") + 1) == 2){
            $myLink = "/en/clinics/";
        }
        else{
            $myLink = "/clinics/";
        }
        ?>
        </div>
        <div class="clear"></div>
        <a href="<?echo $myLink?>" class="btn more-clinic"><?echo GetMessage("TOP_CLINICS")?></a>
    </div>
    <div class="clear"></div>
</div>
<?
