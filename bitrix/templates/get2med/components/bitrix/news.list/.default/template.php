<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
file_put_contents($_SERVER['DOCUMENT_ROOT'].'/clinics.txt', print_r($arResult, true));
$arClinic = array("клиника","клиники","клиник");
function getNumEnding($number, $endingArray)
{
    $number = $number % 100;
    if ($number>=11 && $number<=19) {
        $ending=$endingArray[2];
    }
    else {
        $i = $number % 10;
        switch ($i)
        {
            case (1): $ending = $endingArray[0]; break;
            case (2):
            case (3):
            case (4): $ending = $endingArray[1]; break;
            default: $ending=$endingArray[2];
        }
    }
    return $ending;
}
?>
<h1 class="page-title content">
    <strong>Клиники Кореи</strong>
    <span class="count"><?=count($arResult["ITEMS"]);?> <?=getNumEnding(count($arResult["ITEMS"]),$arClinic);?></span>
</h1>
<div class="main content">
    <div class="sort spacer">
        <!-- <div class="select-list">
            	<div class="section">
                    <select>
                        <option>Сортировка</option>
                        <option>Сортировка1</option>
                        <option>Сортировка2</option>
                        <option>Сортировка3</option>
                        <option>Сортировка4</option>
                    </select>
                </div>

        </div>

        <ul class="view-type">
            <li class="i1"><span></span>Список</li>
            <li class="i2 active"><span></span>Плитка</li>
            <li class="i3"><span></span>Карта</li>
        </ul> -->

        <ul class="view-number">
        	<li>показать по</li>
            <li class="active">10</li>
            <li>20</li>
            <li>30</li>
            <li>40</li>
        </ul><!--view-number-->

    </div><!--sort-->

    <div class="clinics-listing">
    	<?foreach ($arResult["ITEMS"] as $arItem):?>
    		<?if ($arItem["PROPERTIES"]["VIP_CLINIC"]["VALUE"] == "Y"):?>
				<div class="item-vip">
		            <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"/>
		            <div class="item-content box">
		                <p class="item-name"><?=$arItem["NAME"];?></p>
		                <div class="item-raring">
							<?
								$StarRating = round(($arItem["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] +
													 $arItem["PROPERTIES"]["PRICE_QUALITY"]["VALUE"] +
													 $arItem["PROPERTIES"]["OPERATIV"]["VALUE"] +
													 $arItem["PROPERTIES"]["USER_RATING"]["VALUE"])/4);
								$NotStarRating = 5-$StarRating;
							?>
							<?for ($RaringCount=0; $RaringCount < $StarRating; $RaringCount++):?>
								<i class="icon ico-star-wht-slt"></i>
							<?endfor;?>
							<?for ($RaringCount=0; $RaringCount < $NotStarRating; $RaringCount++):?>
								<i class="icon ico-star-wht"></i>
							<?endfor;?>
		                </div><!--raring-->

		                <p class="item-service">
		                    <i class="icon ico-service-white"></i>
		                    <span class="text"><?=$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"]["0"]]["NAME"]?></span>
		                </p>

		                <p class="item-city">
		                    <i class="icon ico-mark-white"></i>
		                    <span class="text">г. <?=$arItem["PROPERTIES"]["TOWN"]["VALUE"]?></span>
		                </p>

		                <p class="item-price">
		                    <i class="icon ico-price-white"></i>
		                    <span class="text">от <?=$arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]?> руб.</span>
		                </p>

		                <p class="item-desc">
		                    <?
	                    	$directionCountmax = count($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"]);
	                    	$directionCount = 0;
	                    	?>
	                    	<?foreach ($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"] as $direction) {
	                    		if ($directionCountmax-1 == $directionCount)
	                    			echo $direction["NAME"];
	                    		else {
	                    			echo $direction["NAME"].", ";
	                    			$directionCount++;
	                    		}
	                    	}?>
		                </p>

		                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn">Подробнее</a>

		            </div><!--item-content--> 
		        </div><!--item-vip-->
				<span class="howto-get-vip">VIP-размещение. <a href="#">Как сюда попасть?</a></span>
			<?break;?>
			<?endif;?>
		<?endforeach;?>

        <div class="spacer">
        	<?foreach ($arResult["ITEMS"] as $arItem):?>
	            <div class="item">
	                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/>
	                <div class="item-content">
	                    <p class="item-name"><?=$arItem["NAME"]?></p>
	                    <div class="item-raring">
							<?
								$StarRating = round(($arItem["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] +
													 $arItem["PROPERTIES"]["PRICE_QUALITY"]["VALUE"] +
													 $arItem["PROPERTIES"]["OPERATIV"]["VALUE"] +
													 $arItem["PROPERTIES"]["USER_RATING"]["VALUE"])/4);
								$NotStarRating = 5-$StarRating;
							?>
							<?for ($RaringCount=0; $RaringCount < $StarRating; $RaringCount++):?>
								<i class="icon ico-star-org-slt"></i>
							<?endfor;?>
							<?for ($RaringCount=0; $RaringCount < $NotStarRating; $RaringCount++):?>
								<i class="icon ico-star-org"></i>
							<?endfor;?>
	                    </div><!--raring-->

	                    <p class="item-service">
	                        <i class="icon ico-service-blue"></i>
	                        <span class="text"><?=$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"]["0"]]["NAME"]?></span>
	                    </p>

	                    <p class="item-city">
	                        <i class="icon ico-mark-blue"></i>
	                        <span class="text">г. <?=$arItem["PROPERTIES"]["TOWN"]["VALUE"]?></span>
	                    </p>

	                    <p class="item-price">
	                        <i class="icon ico-price-blue"></i>
	                        <span class="text">от <?=$arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]?> руб.</span>
	                    </p>

	                    <p class="item-desc">
	                    	<?
	                    	$directionCountmax = count($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"]);
	                    	$directionCount = 0;
	                    	?>
	                    	<?foreach ($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"] as $direction) {
	                    		if ($directionCountmax-1 == $directionCount)
	                    			echo $direction["NAME"];
	                    		else {
	                    			echo $direction["NAME"].", ";
	                    			$directionCount++;
	                    		}
	                    	}?>
	                    </p>

	                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn">Подробнее</a>

	                </div><!--item-content-->
	            </div><!--item-->
	        <?endforeach;?>
        </div><!--spacer-->

    </div><!--clinics-listing-->


    <div class="main-footer spacer">
        <ul class="page-listing">
            <li><a href="#" class="next"><</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#" class="active">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li class="bd-fix"><a href="#">5</a></li>
            <li><a href="#" class="prev">></a></li>
        </ul>

        <ul class="view-number">
            <li class="active">10</li>
            <li>20</li>
            <li>30</li>
            <li>40</li>
            <li>Все</li>
        </ul><!--view-number-->

    </div><!--main-footer-->

</div><!--main-->
