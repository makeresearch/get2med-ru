<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$tmp = substr($_SERVER["REQUEST_URI"], 0, 3);
if ($tmp == "/en"){
    $myPath = "/en";
}
else{
    $myPath = "";
}
?>
<div class="best-box">
    <div class="content">
        <h2><?echo GetMessage("WEEK_BETTER")?></h2>
        <div class="text col-left">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => $myPath."/include/index_inc_advantages_left.php"
                )
            );?>
        </div>
        <div class="text col-right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => $myPath."/include/index_inc_advantages_right.php"
                )
            );?>
        </div>
        <div class="clear"></div>
        <div class="clinics-listing">
        <?foreach ($arResult["ITEMS"] as $arItem):
            $ratingViewed = $arItem["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] + $arItem["PROPERTIES"]["PRICE_QUALITY"]["VALUE"];
            $ratingViewed += $arItem["PROPERTIES"]["OPERATIV"]["VALUE"] + $arItem["PROPERTIES"]["USER_RATING"]["VALUE"];
            $ratingViewed = $ratingViewed/4;
            $ratingViewed = round($ratingViewed, 1);
            $tmpService = CIBlockElement::GetById($arItem["PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"][0]);
            $tmpService = $tmpService->getNext();
            $arItem['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"] = $tmpService["NAME"];?>
            <div class="sticker"><?echo GetMessage("WEEK_NAME")?></div>
            <div class="item-vip">
                <img src="<?echo $arItem["DETAIL_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>" style="width: 1170px; height: 350px">
    
                <div class="item-content box">
                    <p class="item-name"><?echo $arItem["NAME"]?></p>
                    <div class="item-raring">
                         <?$i = 0;
                        for ($i = 0; $i < intVal($ratingViewed); $i ++){?>
                            <i class="icon ico-star-org-slt"></i>
                        <?}
                        for ($j = $i; $j < 5; $j ++){?>
                            <i class="icon ico-star-org"></i>
                        <?}?>
                    </div>
                    <p class="item-service">
                        <i class="icon ico-service-blue"></i>
                        <span class="text"><?echo $arItem['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"]?></span>
                    </p>
                    <p class="item-city">
                        <i class="icon ico-mark-blue"></i>
                        <span class="text"><?echo GetMessage("WEEK_CITY") . $arItem["PROPERTIES"]["TOWN"]["VALUE"]?></span>
                    </p>
                    <p class="item-price">
                        <i class="icon ico-price-blue"></i>
                        <span class="text"><?echo GetMessage("WEEK_PRICE", Array("#TOP_PRICE" => $arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]))?></span>
                    </p>
                    <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="btn"><?echo GetMessage("WEEK_DETAILS")?></a>
    
                </div><!--item-content-->
                
            </div>
            <?endforeach;?>
        </div>
    </div>
</div>