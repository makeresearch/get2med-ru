<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
file_put_contents($_SERVER['DOCUMENT_ROOT'].'/directions.txt', print_r($arResult["ITEMS"], true));
$thisType = 0;
$count = 1;
?>
<div class="item i1">
	<div class="h"><span></span><?echo GetMessage("DIR_SUMMARY")?></div>
	<?foreach ($arResult["ITEMS"] as $direction):?>
		<?foreach ($direction["PROPERTIES"]["DIRECTION_TYPE"]["VALUE_XML_ID"] as $directionType):?>
			<?if ($directionType == 'COMMON') {
				$thisType = 1;
			}?>
		<?endforeach;?>		
	    <?if($thisType == 1):?>
		    <div class="box">
		    	<input type="checkbox" id="checkbox1-<?=$count?>" name="name[]" value="<?=$direction["ID"]?>" />
		        <label for="checkbox1-<?=$count?>"><?=$direction["NAME"]?></label>
		    </div>
		    <?$count++;
		    $thisType = 0;?>
	    <?endif;?>
    <?endforeach;?>
</div>
<?$count = 0;?>
<div class="item i2">
	<div class="h"><span></span><?echo GetMessage("DIR_WOOMAN")?></div>
    <?foreach ($arResult["ITEMS"] as $direction):?>
		<?foreach ($direction["PROPERTIES"]["DIRECTION_TYPE"]["VALUE_XML_ID"] as $directionType):?>
			<?if ($directionType == 'FEMALE') {
				$thisType = 1;
			}?>
		<?endforeach;?>		
	    <?if($thisType == 1):?>
		    <div class="box">
		    	<input type="checkbox" id="checkbox2-<?=$count?>" name="name[]" value="<?=$direction["ID"]?>" />
		        <label for="checkbox2-<?=$count?>"><?=$direction["NAME"]?></label>
		    </div>
		    <?$count++;
		    $thisType = 0;?>
	    <?endif;?>
    <?endforeach;?>
</div>
<?$count = 0;?>
<div class="item i3">
	<div class="h"><span></span><?echo GetMessage("DIR_MAN")?></div>
    <?foreach ($arResult["ITEMS"] as $direction):?>
		<?foreach ($direction["PROPERTIES"]["DIRECTION_TYPE"]["VALUE_XML_ID"] as $directionType):?>
			<?if ($directionType == 'MALE') {
				$thisType = 1;
			}?>
		<?endforeach;?>		
	    <?if($thisType == 1):?>
		    <div class="box">
		    	<input type="checkbox" id="checkbox3-<?=$count?>" name="name[]" value="<?=$direction["ID"]?>" />
		        <label for="checkbox3-<?=$count?>"><?=$direction["NAME"]?></label>
		    </div>
			<?$count++;
		    $thisType = 0;?>
	    <?endif;?>
    <?endforeach;?>
</div>
<?$count = 0;?>
<div class="item i4">
	<div class="h"><span></span><?echo GetMessage("DIR_CHILD")?></div>
    <?foreach ($arResult["ITEMS"] as $direction):?>
		<?foreach ($direction["PROPERTIES"]["DIRECTION_TYPE"]["VALUE_XML_ID"] as $directionType):?>
			<?if ($directionType == 'CHILDREN') {
				$thisType = 1;
			}?>
		<?endforeach;?>		
	    <?if($thisType == 1):?>
		    <div class="box">
		    	<input type="checkbox" id="checkbox4-<?=$count?>" name="name[]" value="<?=$direction["ID"]?>" />
		        <label for="checkbox4-<?=$count?>"><?=$direction["NAME"]?></label>
		    </div>
		    <?$count++;
		    $thisType = 0;?>
	    <?endif;?>
    <?endforeach;?>
</div>