<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?if(!empty($arResult["ITEMS"])):?>
<script type="text/javascript">
    $('.review-item').show();
</script>
    <div class="review bg-full-width bg-2" id="review">
        <div class="content">
            <div class="left-block">
                <ul class="slider slider-img">
                	<?foreach ($arResult["ITEMS"] as $arItem):?>
                        <?$avatar = webImage('avatar-review', $arItem["PREVIEW_PICTURE"]["SRC"]);?>
    	                <li><img src="<?echo $avatar["SRC"]?>" alt="<?echo $arItem["NAME"]?>"/></li>
                    <?endforeach;?>
                </ul>
                <div class="slider-control">
                    <span class="pren" id="slider-img-back"></span>
                    <span class="next" id="slider-img-next"></span>
                </div><!--slider-control-->
            </div><!--left-block-->

            <h2><?echo GetMessage("TESTIMONIALS")?></h2>

            <div class="rev-text">
                <div class="slider-text">
                	<?foreach ($arResult["ITEMS"] as $arItem):?>
    	                <div class="item">
                            <?if(isset($arItem["DISPLAY_ACTIVE_FROM"]) AND !empty($arItem["DISPLAY_ACTIVE_FROM"])){?>
                                <p class="date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></p>
                            <?}?>
                            <?if(isset($arItem["PROPERTIES"]["CLINIC"]["CLINIC_NAME"]) AND !empty($arItem["PROPERTIES"]["CLINIC"]["CLINIC_NAME"])){?>
    	                        <p class="name"><?echo $arItem["NAME"]." о «".$arItem["PROPERTIES"]["CLINIC"]["CLINIC_NAME"]."»"?></p>
                            <?} else {?>
                                <p class="name"><?echo $arItem["NAME"]?></p>
                            <?}?>
    	                    <p class="text">
    	                        <?echo $arItem["PREVIEW_TEXT"];?>
    	                    </p>

    	                    <p class="rating">
    	                        <?echo GetMessage("MY_MARK", Array("#MARK_VALUE" => $arItem["PROPERTIES"]["MARK"]["VALUE"]))?>
    	                    </p>
    	                </div>
                    <?endforeach;?>
                    
                </div>

            </div><!--rev-text-->

        </div><!--content-->

    </div><!--review-->
<?endif;?>