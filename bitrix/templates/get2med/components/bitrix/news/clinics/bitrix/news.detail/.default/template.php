<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (session_start()){
}
$engTest = substr($_SERVER["REQUEST_URI"], 0, 3);

if ((strpos($engTest, "en")+1) == 2){
	$arViewed["VALUE"] = $_SESSION["VIEWED_EN"];
	$engTest = "EN";
}
else{
	$arViewed["VALUE"] = $_SESSION["VIEWED_RU"];
	$engTest = "RU";
}
$ratingClinic = $arResult["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] + $arResult["PROPERTIES"]["PRICE_QUALITY"]["VALUE"];
$ratingClinic += $arResult["PROPERTIES"]["OPERATIV"]["VALUE"] + $arResult["PROPERTIES"]["USER_RATING"]["VALUE"];
$ratingClinic = $ratingClinic/4;
$ratingClinic = round($ratingClinic, 1);
//background in header
$header_background = $arResult["DETAIL_PICTURE"]["SRC"];
if(empty($header_background)) { $header_background = SITE_TEMPLATE_PATH . '/images/week-clinic-bg.jpg'; }
//logotype
$arLogo = webImage('clinic-logo', CFile::getPath($arResult["PROPERTIES"]["LOGO"]["VALUE"]));
?>


<div class="header bg-full-width" style="background-image: url('<?php echo $header_background?>');">
	<?//echo '[',$arResult["DETAIL_PICTURE"]["SRC"]?>
	<div class="content box">
		<div class="clinic-name">
			<?if (!empty($arResult["PROPERTIES"]["LOGO"]["VALUE"])){?>
				<img src="<?php echo $arLogo['SRC']?>" width="<?php echo $arLogo['WIDTH']; ?>" height="<?php echo $arLogo['HEIGHT']; ?>" alt="<?echo $arResult["NAME"]?>"/>
			<?}?>
			<h1><?echo $arResult["NAME"]?></h1>

			<div class="raring">
				<?$i = 0;
				for ($i = 0; $i < intVal($ratingClinic); $i ++){?>
					<i class="icon ico-star-wht-slt"></i>
				<?}
				for ($j = $i; $j < 5; $j ++){?>
					<i class="icon ico-star-wht"></i>
				<?}?>
				<span class="desc"><?echo GetMessage("RATING", Array("#COUNT" => $ratingClinic))?></span>
			</div><!--raring-->

		</div><!--clinic-name-->

		<div class="callback">
			<p>8 (800) 333-14-92</p>
			<button onClick="location.href='#konsultation'"><?echo GetMessage("KONSULTATION")?></button>

		</div><!--callback-->

		<div class="clinic-desc">
			<?if(!empty($arResult['PROPERTIES']['DIRECTIONS_FILTER']['ITEMS'])):?>
				<p class="service">
					<i class="icon ico-service-white"></i>
					<span class="text">
						<?
						$boolFirstDirect = true;
						foreach ($arResult['PROPERTIES']['DIRECTIONS_FILTER']['ITEMS'] as $value) {
							
							if ($boolFirstDirect){
								echo $value["NAME"];
							}
							else{
								echo ", ".$value["NAME"];
							}
							$boolFirstDirect = false;
						}?>
					</span>
				</p>
			<?endif;?>
			<?if(!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"])):?>
				<p class="city">
					<i class="icon ico-mark-white"></i>
					<span class="text"><?echo $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]?></span>
				</p>
			<?endif;?>
			<?if(!empty($arResult["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"])):?>
				<p class="price">
					<i class="icon ico-price-white"></i>
					<span class="text"><?echo $arResult["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]." ".GetMessage("RUB")?></span>
				</p>
			<?endif;?>
		</div><!--clinic-desc-->
		<!--
		<div class="add-favorites">
			<i class="icon ico-favorites"></i>
			<span>Добавить в избранное</span>
		</div>-->

	</div><!--content-->

</div><!--header-->

<div class="page-nav content">
	<ul>
		<li><a class="smooth" href="#about"><?echo GetMessage("ABOUT")?></a></li>
		<li><a class="smooth" href="#prices"><?echo GetMessage("PRICES")?></a></li>
		<li class="doctor-item" style="display:none"><a class="smooth" href="#doctor"><?echo GetMessage("DOCTORS")?></a></li>
		<li class="photo-item" style="display:none"><a class="smooth" href="#photo"><?echo GetMessage("PHOTOGALLERY")?></a></li>
		<li class="video-item" style="display:none"><a class="smooth" href="#video"><?echo GetMessage("VIDEO")?></a></li>
		<li class="review-item" style="display:none"><a class="smooth" href="#review"><?echo GetMessage("REVIEWS")?></a></li>
		<li><a class="smooth" href="#contacts"><?echo GetMessage("CONTACTS")?></a></li>
		<li><button class="konsButton" style="display: none;" onClick="location.href='#konsultation'"><?echo GetMessage("KONSULTATION")?></button></li>
	</ul>
	
</div><!--page-nav-->

<div class="about-us content" id="about">
	<div class="left-block">
		<h2><?echo GetMessage("ABOUT")?></h2>
		<p class="mb-fix">
			<?echo $arResult["DETAIL_TEXT"]?>
		</p>
		<div class="social">
			<?if (!empty($arResult["PROPERTIES"]["LINK_FACEBOOK"]["VALUE"])):?>
				<a href="<?echo $arResult["PROPERTIES"]["LINK_FACEBOOK"]["VALUE"]?>"><i class="icon ico-facebook-bg"></i></a>
			<?endif?>
			<?if (!empty($arResult["PROPERTIES"]["LINK_VK"]["VALUE"])):?>
				<a href="<?echo $arResult["PROPERTIES"]["LINK_VK"]["VALUE"]?>"><i class="icon ico-vk-bg"></i></a>
			<?endif?>
			<?if (!empty($arResult["PROPERTIES"]["LINK_TWITTER"]["VALUE"])):?>
				<a href="<?echo $arResult["PROPERTIES"]["LINK_TWITTER"]["VALUE"]?>"><i class="icon ico-twitter-bg"></i></a>
			<?endif?>
			<?if (!empty($arResult["PROPERTIES"]["LINK_GOOGLE_PLUS"]["VALUE"])):?>
				<a href="<?echo $arResult["PROPERTIES"]["LINK_GOOGLE_PLUS"]["VALUE"]?>"><i class="icon ico-google-bg"></i></a>
			<?endif?>

		</div><!--social-->

	</div><!--left-block-->

	<div class="ratings">
		<h3><?echo GetMessage("CLINIC_RATING")?></h3>

		<ul>
			<li>
				<span class="name"><?echo $arResult["PROPERTIES"]["DEGREE_OF_SERVICE"]["NAME"]?></span>
				<span class="desc">
					<?$i = 0;
					for ($i = 0; $i < $arResult["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"]; $i ++){?>
						<i class="icon ico-star-org-slt"></i>
					<?}
					for ($j = $i; $j < 5; $j ++){?>
						<i class="icon ico-star-org"></i>
					<?}?>
				</span>
			</li>

			<li>
				<span class="name"><?echo $arResult["PROPERTIES"]["PRICE_QUALITY"]["NAME"]?></span>
				<span class="desc">
					<?$i = 0;
					for ($i = 0; $i < $arResult["PROPERTIES"]["PRICE_QUALITY"]["VALUE"]; $i ++){?>
						<i class="icon ico-star-org-slt"></i>
					<?}
					for ($j = $i; $j < 5; $j ++){?>
						<i class="icon ico-star-org"></i>
					<?}?>
				</span>
			</li>
			<li>
				<span class="name"><?echo $arResult["PROPERTIES"]["OPERATIV"]["NAME"]?></span>
				<span class="desc">
					<?$i = 0;
					for ($i = 0; $i < $arResult["PROPERTIES"]["OPERATIV"]["VALUE"]; $i ++){?>
						<i class="icon ico-star-org-slt"></i>
					<?}
					for ($j = $i; $j < 5; $j ++){?>
						<i class="icon ico-star-org"></i>
					<?}?>
				</span>
			</li>
			<?/*
			<li>
				<span class="name"><?echo $arResult["PROPERTIES"]["USER_RATING"]["NAME"]?></span>
				<span class="desc">
					<?$i = 0;
					for ($i = 0; $i < $arResult["PROPERTIES"]["USER_RATING"]["VALUE"]; $i ++){?>
						<i class="icon ico-star-org-slt"></i>
					<?}
					for ($j = $i; $j < 5; $j ++){?>
						<i class="icon ico-star-org"></i>
					<?}?>
				</span>
			</li>
			*/?>
			<li>
				<strong class="name"><?echo GetMessage("SUMMARY_RATING")?></strong>
				<span class="desc">
					<?$i = 0;
					$ratingClinic = intval($ratingClinic);
					for ($i = 0; $i < $ratingClinic; $i ++){?>
						<i class="icon ico-star-org-slt"></i>
					<?}
					for ($j = $i; $j < 5; $j ++){?>
						<i class="icon ico-star-org"></i>
					<?}?>
				</span>
			</li>

		</ul>

	</div><!--ratings-->

</div><!--about-us-->

<div class="service-price content" id="prices">
	<div class="spacer">
		<h2><?echo GetMessage("PRICES")?></h2>
		<?
		$RUSmoney = 0;
		$USDmoney = 0;
		$EURmoney = 0;
		$KRWmoney = 0;
		?>
		<ul class="currency-listing">
			<li class="active"><a data-id="RUS" >RUB</a></li>
			<li><a data-id="USD">USD</a></li>
			<li><a data-id="EUR">EUR</a></li>
			<li><a data-id="KRW">KRW</a></li>
		</ul>

	</div><!--spacer-->
	<div class="accordion">
		<?
		$boolIsFirstDirect = false;
		foreach ($arResult["PROPERTIES"]["DIRECTIONS_DESCRIPTION"]["DIRECTIONS"] as $arSect):
			//if ($boolIsFirstDirect) {
			if(count($arSect["SERVICES"]) == 0) { ?>
				<div class="item disabled"></i>
			<? } else { ?>
				<div class="item close"></i>
			<?  } ?>
				<p class="title">
					<? if(count($arSect["SERVICES"]) != 0) { ?>
						<? if ($boolIsFirstDirect) { ?>
							<i class="icon ico-close"></i>
						<? } else { ?>
							<i class="icon ico-open"></i>
						<? }
					} else {
						print '<i class="icon" style="margin-right: 19px;"></i>';
					}
						$boolIsFirstDirect = false;?>

					<span><?echo $arSect["NAME"]?></span>
				</p>
				<? if(count($arSect["SERVICES"]) > 0) { ?>
					<div class="item-content">
						<?if(!empty($arSect["DESCRIPTION"])) {?>
							<p><?echo $arSect["DESCRIPTION"]?></p>
						<? } ?>
						<ul>
							<?foreach ($arSect["SERVICES"] as $arItem):?>
								<li>
									<span class="name"><?echo $arItem["NAME"]?></span>
									<?if(!empty($arItem["PROPERTY_PRICE_RUB_VALUE"])):?>
										<?$RUSmoney=1;?>
										<span class="price" style="" data-id="RUS"><?echo $arItem["PROPERTY_PRICE_RUB_VALUE"]?> RUB</span>
									<?endif;?>
									<?if(!empty($arItem["PROPERTY_PRICE_USD_VALUE"])):?>
										<?$USDmoney=1;?>
										<span class="price" style="display: none" data-id="USD"><?echo $arItem["PROPERTY_PRICE_USD_VALUE"]?> USD</span>
									<?endif;?>
									<?if(!empty($arItem["PROPERTY_PRICE_EUR_VALUE"])):?>
										<?$EURmoney=1;?>
										<span class="price" style="display: none" data-id="EUR"><?echo $arItem["PROPERTY_PRICE_EUR_VALUE"]?> EUR</span>
									<?endif;?>
									<?if(!empty($arItem["PROPERTY_PRICE_KRW_VALUE"])):?>
										<?$KRWmoney=1;?>
										<span class="price" style="display: none" data-id="KRW"><?echo $arItem["PROPERTY_PRICE_KRW_VALUE"]?> KRW</span>
									<?endif;?>
								</li>
							<?endforeach;?>
						</ul>
					</div><!--item-content-->
				<? } ?>
			</div><!--item-->
		<?endforeach;?>
	</div><!--accordion-->
</div><!--service-price-->
<?if(!empty($arResult["PROPERTIES"]["DOCTORS"]["ITEMS"])):?>
<script type="text/javascript">
	$('.doctor-item').show();
</script>
<div class="clinic-doctors content" id="doctor">
	<h2><?echo GetMessage("DOCTORS")?></h2>
	<div class="listing spacer left-based">
		<?
		$currShowCount = 8;
		$doctorCount = 0;
		foreach ($arResult["PROPERTIES"]["DOCTORS"]["ITEMS"] as $arItem):?>
		<div class="item">
			<img src="<?echo $arItem["PREVIEW_SRC"]?>" alt="<?echo $arItem["NAME"]?>"/>
			<p class="name"><?echo $arItem["NAME"]?></p>
			<p class="service"><?echo $arItem["PROPERTIES"]["SPECIALIZATION"]["VALUE"]?></p>
			<p class="desc">
				<?echo $arItem["PROPERTIES"]["LENGTH_OF_WORK"]["NAME"].": ".$arItem["PROPERTIES"]["LENGTH_OF_WORK"]["VALUE"]." ".GetMessage("YEAR")?><br/>
				<?
				$boolIsFirstHonor = true;
				foreach ($arItem["PROPERTIES"]["HONORS"]["VALUE"] as $honor):
					if ($boolIsFirstHonor){
						echo $honor;
					} else{
						echo "<br />".$honor;
					}
					$boolIsFirstHonor = false;
				endforeach?>
			</p>
		</div><!--item-->

		<?
		$doctorCount ++;
		endforeach;?>
	</div><!--listing-->
	<?
	if ($doctorCount > $currShowCount){?>
		<button class="view-all"><?echo GetMessage("MORE_DOCTORS")?></button>

	<?
		$currShowCount += 8;
	}?>
</div><!--clinic-doctors-->
<?endif;?>
<?if(!empty($arResult["PROPERTIES"]["GALLERY"]["VALUE"])):?>
<script type="text/javascript">
	$('.photo-item').show();
</script>
<div class="gallery content" id="photo">
	<h2><?echo GetMessage("PHOTOGALLERY")?></h2>

	<ul class="slider gallery-slider">
		<?$currCount = 0;
		$boolIsFirstPhoto = true;
		foreach ($arResult["PROPERTIES"]["GALLERY"]["VALUE"] as $onePhoto):
			if ($boolIsFirstPhoto):
				$boolIsFirstPhoto = false;
				echo "<li class=\"spacer\">";
			endif;?>
			<?$NewImg = webImage('clinics', CFile::GetPath($onePhoto));?>
			<a href="<?echo CFile::GetPath($onePhoto)?>" class="fancybox" rel="gallery"><img src="<?=$NewImg["FILE_NAME"]?>" alt="Image"/></a>
			
			<?$currCount ++;
			if ($currCount == 4):
				$boolIsFirstPhoto = true;
				$currCount = 0;
				echo "</li>";
			endif;
		endforeach;
		if ($currCount < 4){
			echo "</li>";
		}?>
	</ul>

	<div class="slider-control">
		<span class="pren" id="slider-gallery-back"></span>
		<span class="next" id="slider-gallery-next"></span>
	</div><!--slider-control-->

</div><!--gallery-->
<?endif;?>
<?if(!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])):?>
<script type="text/javascript">
	$('.video-item').show();
</script>
<div class="video bg-full-width" id="video">
	<?echo html_entity_decode($arResult["PROPERTIES"]["VIDEO"]["VALUE"])?>
	<!--
	<div class="video-play pos-center">

		<span class="icon-wrap"><i class="icon ico-play"></i></span>
		<span class="text">Видео о клинике</span>
		
	</div><!--video-play-->

</div><!--video-->
<?endif;?>
<?
$GLOBALS["revFilter"] = array('PROPERTY_35' => $arResult["ID"]);

$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"detailPageReviews",
	Array(
		"IBLOCK_TYPE" => "-",
		"IBLOCK_ID" => "13",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"USE_FILTER" => "Y",
		"FILTER_PROPERTY_CODE" => array(
		   0 => "CLINIC",
		  ),
		"FILTER_NAME" => "revFilter",
		"FIELD_CODE" => array("PREVIEW_PICTURE", "ACTIVE_FROM", ""),
		"PROPERTY_CODE" => array("MARK", "CLINIC", ""),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N"
	)
);?>
<div class="form-block bg-full-width" id="konsultation">
	<div class="content">
		<h3>
			<?echo GetMessage("CONSULT_QUEST", Array ("#NAME" => $arResult["NAME"]))?>
		</h3>
		<?$APPLICATION->IncludeComponent(
			"custom:main.feedback",
			"detailPageFeedback",
			Array(
				"USE_CAPTCHA" => "N",
				"OK_TEXT" => "Спасибо, ваше сообщение принято.",
				"EMAIL_TO" => "",
				"REQUIRED_FIELDS" => array("MAIL", "PHONE"),
				"EVENT_MESSAGE_ID" => array("7"),
				"ADD_SERVICES" => $arResult["PROPERTIES"]["ADDITIONAL_SERVICES"]["VALUE"],
                "AJAX_MODE" => 'Y'
			)
		);?>
   </div><!--content-->
</div><!--form-block-->


<div class="map-wrap bg-full-width" id="contacts">
	<div id="map" class="bg-full-width"></div>
	<div class="content">
		<div class="contacts">
			<h2><?echo GetMessage("CONTACTS")?></h2>

			<ul>
				<?if(!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"])):?>
					<li>
						<p class="name"><?echo $arResult["PROPERTIES"]["ADDRESS"]["NAME"]?></p>
						<p class="desc">
							<?echo $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]?>
						</p>
					</li>
				<?endif;?>
				<?if(!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"])):?>
					<li>
						<p class="name"><?echo $arResult["PROPERTIES"]["PHONE"]["NAME"]?></p>
						<p class="desc">
							<?echo $arResult["PROPERTIES"]["PHONE"]["VALUE"]?>
						</p>
					</li>
				<?endif;?>
				<?if(!empty($arResult["PROPERTIES"]["PHONE_SERVICE"]["VALUE"])):?>
					<li>
						<p class="name"><?echo $arResult["PROPERTIES"]["PHONE_SERVICE"]["NAME"]?></p>
						<p class="desc">
							<?echo $arResult["PROPERTIES"]["PHONE_SERVICE"]["VALUE"]?>
						</p>
					</li>
				<?endif;?>
				<?if(!empty($arResult["PROPERTIES"]["WORK_GRAPH"]["VALUE"])):?>
					<li>
						<p class="name"><?echo $arResult["PROPERTIES"]["WORK_GRAPH"]["NAME"]?></p>
						<p class="desc">
							<?echo $arResult["PROPERTIES"]["WORK_GRAPH"]["VALUE"]?>
						</p>
					</li>
				<?endif;?>
			</ul>

		</div><!--contacts-->

	</div><!--content-->
</div><!--contacts-->

<?if(isset($arViewed["VALUE"]) AND !empty($arViewed["VALUE"])){?>
<div class="viewed content">
	<h2><?echo GetMessage("VIEWED")?></h2>
	<div class="clinics-listing">
		<?
		$counter = 0;
		foreach ($arViewed["VALUE"] as $arViewedItem):
			
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_TOWN", "PROPERTY_USER_RATING", "PROPERTY_OPERATIV", "PROPERTY_PRICE_QUALITY",
			 "PROPERTY_DEGREE_OF_SERVICE", "PROPERTY_COST_OF_SERVICES", "PROPERTY_DIRECTIONS_DESCRIPTION", "DETAIL_PAGE_URL");
			$arFilter = Array("ID"=>IntVal($arViewedItem), "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
			
			while ($arFld = $res->getNextElement()){
			  $arViewed['ITEMS'][$counter] = $arFld->getFields();
			  $arViewed['ITEMS'][$counter]["PROPERTIES"] = $arFld->getProperties();
			  
			}
			foreach ($arViewed['ITEMS'][$counter]['PROPERTIES']['DIRECTIONS_FILTER']['VALUE'] as $value) {
				$direction = CIBlockElement::GetByID($value);
				$direction = $direction->GetNext();
				$arViewed['ITEMS'][$counter]['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"][] = $direction["NAME"];
			}
			
			$arViewed["ITEMS"][$counter]["PREVIEW_SRC"] = CFile::GetPath($arViewed["ITEMS"][$counter]["PREVIEW_PICTURE"]);
			$counter ++;
		endforeach;
		$arSelect = Array("ID", "IBLOCK_ID", "NAME");
		$arFilter = Array("ID"=>IntVal($arViewedItem), "ACTIVE"=>"Y");
	   
		foreach ($arViewed["ITEMS"] as $arItem):?>
			<div class="item">

                <a class="img" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <?$NewImg = webImage('clinics', $arItem["PREVIEW_SRC"]);?>
                    <img src="<?=$NewImg["FILE_NAME"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/>
                </a>
				<?
				$ratingViewed = $arItem["PROPERTY_DEGREE_OF_SERVICE_VALUE"] + $arItem["PROPERTY_PRICE_QUALITY_VALUE"];
				$ratingViewed += $arItem["PROPERTY_OPERATIV_VALUE"] + $arItem["PROPERTY_USER_RATING_VALUE"];
				$ratingViewed = $ratingViewed/4;
				$ratingViewed = round($ratingViewed, 1);
				?>
				<div class="item-content">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-name"><?=$arItem["NAME"]?></a>
					<div class="item-raring">
						 <?$i = 0;
						for ($i = 0; $i < intVal($ratingViewed); $i ++){?>
							<i class="icon ico-star-org-slt"></i>
						<?}
						for ($j = $i; $j < 5; $j ++){?>
							<i class="icon ico-star-org"></i>
						<?}?>
						<span class="desc"><?echo GetMessage("RATING", Array("#COUNT" => $ratingViewed))?></span>

					</div><!--raring-->

					<p class="item-service">
						<i class="icon ico-service-blue"></i>
						<?$boolFirstService = true;
						foreach ($arItem['PROPERTIES']['DIRECTIONS_FILTER']["NAMES"] as $oneService) {
							if ($boolFirstService){
								$serv = $oneService;
								$boolFirstService = false;
							}
							else{
								$serv .= ", ".$oneService;
							}
						}?>

						<span class="text"><?echo $serv?></span>
					</p>

					<p class="item-city">
						<i class="icon ico-mark-blue"></i>
						<span class="text"><?echo GetMessage("DET_CITY", Array("#CITY" => $arItem["PROPERTY_TOWN_VALUE"]))?></span>
					</p>
					<?if(!empty($arItem["PROPERTY_COST_OF_SERVICES_VALUE"])):?>
						<p class="item-price">
							<i class="icon ico-price-blue"></i>
							<span class="text"><?echo GetMessage("DET_PRICE", Array("#DET_PRICE" => $arItem["PROPERTY_COST_OF_SERVICES_VALUE"]))?></span>
						</p>
					<?endif;?>
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="btn"><?echo GetMessage("MORE");?></a>

				</div><!--item-content-->

			</div><!--item-->
		<?endforeach;?>
		
	</div><!--clinics-listing-->

</div><!--viewed-->
<?}?>
<?//*********************************create viewed clinic list**********************************//
	$countViewedClinic = 5;
	if (!in_array($arResult["ID"], $_SESSION["VIEWED_".$engTest])){
		if ((count($_SESSION["VIEWED_".$engTest]) < $countViewedClinic)||(!isset($_SESSION["VIEWED_".$engTest]))){
			$_SESSION["VIEWED_".$engTest][] = $arResult["ID"];
		}
		else{
			for ($i = 0; $i < $countViewedClinic-1; $i ++){
				$_SESSION["VIEWED_".$engTest][$i] = $_SESSION["VIEWED_".$engTest][$i+1];
			}
			$_SESSION["VIEWED_".$engTest][$countViewedClinic-1] = $arResult["ID"];
		}
	}
 //********************************************************************************************//
?>
<?
	if (!empty($arResult["PROPERTIES"]["CLINIC_MAP"]["VALUE"]))
		$MapMarkerPoint = $arResult["PROPERTIES"]["CLINIC_MAP"]["VALUE"];
	else
		$MapMarkerPoint = '35.16445,129.0718';
?>
<script>
	function initialize() {
		var mapOptions = {
			zoom: 12,
			scrollwheel: false,
			disableDefaultUI: true,
			center: new google.maps.LatLng(<?echo $MapMarkerPoint?>)
		};
		var map = new google.maps.Map(document.getElementById('map'),
				mapOptions);

		var image = '/bitrix/templates/get2med/images/map-mark.png';
		var myLatLng = new google.maps.LatLng(<?echo $MapMarkerPoint?>);
		new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: image
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
///////change current price on view//////////
	$(".service-price .spacer .currency-listing a").on("click", function(){
		$(".service-price .spacer .currency-listing .active").removeClass("active");
		$(this).parent().addClass("active");
		$(".accordion .item-content .price").attr("style", "display: none");
		var prRequest = ".accordion .item-content .price[data-id=" + $(this).attr("data-id") + "]";
		$(prRequest).attr("style", "");
	});
	var RUSmoney = <?=$RUSmoney;?>;
	var USDmoney = <?=$USDmoney;?>;
	var EURmoney = <?=$EURmoney;?>;
	var KRWmoney = <?=$KRWmoney;?>;
	if (RUSmoney == 0) {
		$('a[data-id=RUS]').parent().hide();
	};
	if (USDmoney == 0) {
		$('a[data-id=USD]').parent().hide();
	};
	if (EURmoney == 0) {
		$('a[data-id=EUR]').parent().hide();
	};
	if (KRWmoney == 0) {
		$('a[data-id=KRW]').parent().hide();
	};
</script>