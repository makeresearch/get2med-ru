<?
$MESS["RATING"] = "Рейтинг: #COUNT из 5";
$MESS["KONSULTATION"] = "Заказать консультацию";
$MESS["RUB"] = "руб.";
$MESS["ABOUT"] = "О клинике";
$MESS["PRICES"] = "Направления и лечение";
$MESS["DOCTORS"] = "Врачи";
$MESS["PHOTOGALLERY"] = "Фотогалерея";
$MESS["VIDEO"] = "Видео";
$MESS["REVIEWS"] = "Отзывы";
$MESS["CONTACTS"] = "Контакты";
$MESS["CLINIC_RATING"] = "Рейтинг клиники";
$MESS["SUMMARY_RATING"] = "Общий рейтинг";
$MESS["YEAR"] = "лет";
$MESS["MORE_DOCTORS"] = "Показать еще 8 врачей";
$MESS["CONSULT_QUEST"] = "Если вы хотите обратиться в эту клинику, отправьте нам сообщение, и наш менеджер даст вам полную консультацию по телефону либо e-mail:";
$MESS["VIEWED"] = "Вы уже смотрели";
$MESS["MORE"] = "Подробнее";
$MESS["DET_CITY"] = "г. #CITY";
$MESS["DET_PRICE"] = "От #DET_PRICE руб.";
?>