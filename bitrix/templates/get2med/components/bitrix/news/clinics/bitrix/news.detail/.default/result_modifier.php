<?/*add direction_filter_name to arResult*/
foreach ($arResult['PROPERTIES']['DIRECTIONS_FILTER']['VALUE'] as &$value) {
    $arSelect = Array("ID", "NAME", "IBLOCK_ID");
    $arFilter = Array("ID"=>IntVal($value), "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    $arFld = $res->getNext();
    $arResult['PROPERTIES']['DIRECTIONS_FILTER']['ITEMS'][] = $arFld;
    
}		
/*add direction_description and direction_description_services to arResult*/
$arFilter = Array('IBLOCK_ID'=>$arResult["PROPERTIES"]["DIRECTIONS_DESCRIPTION"]["LINK_IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$arResult['PROPERTIES']['DIRECTIONS_DESCRIPTION']['VALUE']);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
$counter = 0;
while($ar_result = $db_list->GetNext())
{
  $arResult["PROPERTIES"]["DIRECTIONS_DESCRIPTION"]["DIRECTIONS"][$counter] = $ar_result;
  $arFilter = Array('IBLOCK_ID'=>$arResult["PROPERTIES"]["DIRECTIONS_DESCRIPTION"]["LINK_IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$ar_result["ID"]);
  $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_PRICE_RUB", "PROPERTY_PRICE_USD", "PROPERTY_PRICE_EUR", "PROPERTY_PRICE_KRW");
	$directServices = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
	while($ar_services = $directServices->GetNext())
	{
		$arResult["PROPERTIES"]["DIRECTIONS_DESCRIPTION"]["DIRECTIONS"][$counter	]["SERVICES"][] = $ar_services;
	}
  $counter ++;
}
/*add doctors to arResult*/
$counter = 0;

foreach ($arResult['PROPERTIES']['DOCTORS']['VALUE'] as &$value) {
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_SPECIALIZATION", "PROPERTY_LENGTH_OF_WORK", "PROPERTY_HONORS");
    $arFilter = Array("ID"=>IntVal($value), "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    //$arFld = $res->getNext();
    
    
    while ($arFld = $res->getNextElement()){
      $arResult['PROPERTIES']['DOCTORS']['ITEMS'][$counter] = $arFld->getFields();
      $arResult['PROPERTIES']['DOCTORS']['ITEMS'][$counter]["PREVIEW_SRC"] = CFile::GetPath($arResult['PROPERTIES']['DOCTORS']['ITEMS'][$counter]["PREVIEW_PICTURE"]);
      $arResult["PROPERTIES"]["DOCTORS"]["ITEMS"][$counter]["PROPERTIES"] = $arFld->getProperties();
    }
    $counter ++;
} 