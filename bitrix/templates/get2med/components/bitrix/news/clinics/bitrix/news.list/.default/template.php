<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
        <div class="spacer left-based">
        	<?foreach ($arResult["ITEMS"] as $arItem):?>
	            <div class="item">
	                <a class="img" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
	                	<?$NewImg = webImage('clinics', $arItem["PREVIEW_PICTURE"]["SRC"]);?>
	                	<img src="<?=$NewImg["FILE_NAME"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/>
	                </a>
	                <div class="item-content">
	                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-name"><?=$arItem["NAME"]?></a>
	                    <div class="item-raring">
							<?
								$StarRating = round(($arItem["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] +
													 $arItem["PROPERTIES"]["PRICE_QUALITY"]["VALUE"] +
													 $arItem["PROPERTIES"]["OPERATIV"]["VALUE"] +
													 $arItem["PROPERTIES"]["USER_RATING"]["VALUE"])/4);
								$NotStarRating = 5-$StarRating;
							?>
							<?for ($RaringCount=0; $RaringCount < $StarRating; $RaringCount++):?>
								<i class="icon ico-star-org-slt"></i>
							<?endfor;?>
							<?for ($RaringCount=0; $RaringCount < $NotStarRating; $RaringCount++):?>
								<i class="icon ico-star-org"></i>
							<?endfor;?>
	                    </div><!--raring-->
						<?if(!empty($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"]["0"]]["NAME"])):?>
		                    <p class="item-service">
		                        <i class="icon ico-service-blue"></i>
		                        <span class="text"><?=$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"]["0"]]["NAME"]?></span>
		                    </p>
						<?endif;?>
	                    <p class="item-city">
	                        <i class="icon ico-mark-blue"></i>
	                        <span class="text"><?echo GetMessage("LIST_CITY") . $arItem["PROPERTIES"]["TOWN"]["VALUE"]?></span>
	                    </p>
						<?if(!empty($arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"])):?>
	                    <p class="item-price">
	                        <i class="icon ico-price-blue"></i>
	                        <span class="text"><?echo GetMessage("LIST_PRICE", Array("#LIST_PRICE" => $arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]))?></span>
	                    </p>
						<?endif;?>
	                    <p class="item-desc">
	                    	<?
	                    	$directionCountmax = count($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"]);
	                    	$directionCount = 0;
	                    	?>
	                    	<?foreach ($arItem["DISPLAY_PROPERTIES"]["DIRECTIONS_FILTER"]["LINK_ELEMENT_VALUE"] as $direction) {
	                    		if ($directionCountmax-1 == $directionCount)
	                    			echo $direction["NAME"];
	                    		else {
	                    			echo $direction["NAME"].", ";
	                    			$directionCount++;
	                    		}
	                    	}?>
	                    </p>

	                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn"><?echo GetMessage("LIST_READ_MORE")?></a>

	                </div><!--item-content-->
	            </div><!--item-->
	        <?endforeach;?>
        </div><!--spacer-->

    </div><!--clinics-listing-->


    <div class="main-footer spacer">
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
        <div class="number-wrap">
            <ul class="view-number bot">
                <li class="active" value="10">10</li>
                <li value="20">20</li>
                <li value="30">30</li>
                <li value="40">40</li>
                <li value="10000"><?echo GetMessage("LIST_ALL")?></li>
            </ul><!--view-number-->
        </div>

    </div><!--main-footer-->

</div><!--main-->

<script type="text/javascript">
	var clinics_count = '<?=$_SESSION["news_count"];?>';
	if (clinics_count > 10) {
		$('.view-number li').each(function(){
			if ($(this).hasClass("active")) {
				$(this).removeClass("active");
			}
			else {
				if ($(this).val() == clinics_count) {
					$(this).addClass("active");
				};
			}
		});
	};
</script>