<?
$MESS["RATING"] = "Rating: #COUNT of 5";
$MESS["KONSULTATION"] = "Consultation";
$MESS["RUB"] = "RUB";
$MESS["ABOUT"] = "About clinic";
$MESS["PRICES"] = "Directions and prices";
$MESS["DOCTORS"] = "Doctors";
$MESS["PHOTOGALLERY"] = "Photogallery";
$MESS["VIDEO"] = "Video";
$MESS["REVIEWS"] = "Reviews";
$MESS["CONTACTS"] = "Contacts";
$MESS["CLINIC_RATING"] = "Rating of clinic";
$MESS["SUMMARY_RATING"] = "Summary rating";
$MESS["YEAR"] = "years";
$MESS["MORE_DOCTORS"] = "Show more doctors";
$MESS["CONSULT_QUEST"] = "If you would like to go to the clinic <<span class=\"name\">#NAME</span>>, send us a message
            and our manager will give you a full consultation by phone or e-mail:";
$MESS["VIEWED"] = "You now viewed";
$MESS["MORE"] = "Details";
$MESS["DET_CITY"] = "#CITY";
$MESS["DET_PRICE"] = "Then #DET_PRICE RUB";
?>