<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$engTest = substr($_SERVER["REQUEST_URI"], 0, 3);

if ((strpos($engTest, "en")+1) == 2){
    $arViewed["VALUE"] = $_SESSION["VIEWED_EN"];
    $engTest = "EN";
}
else{
    $arViewed["VALUE"] = $_SESSION["VIEWED_RU"];
    $engTest = "RU";
}
session_start();
if ($_SESSION['news_count'])
    $clinics_count = $_SESSION['news_count'];
else
    $clinics_count = 10;
$arClinic = array("клиника","клиники","клиник");
function getNumEnding($number, $endingArray)
{
    $number = $number % 100;
    if ($number>=11 && $number<=19) {
        $ending=$endingArray[2];
    }
    else {
        $i = $number % 10;
        switch ($i)
        {
            case (1): $ending = $endingArray[0]; break;
            case (2):
            case (3):
            case (4): $ending = $endingArray[1]; break;
            default: $ending=$endingArray[2];
        }
    }
    return $ending;
}
if ($engTest == "RU"){
    $dbDirections = CIBlockElement::GetList(array(),array('IBLOCK_ID' => '3'),false,false,array("ID","NAME"));
}
else{
    $dbDirections = CIBlockElement::GetList(array(),array('IBLOCK_ID' => '4'),false,false,array("ID","NAME")); 
}

while ($obDirections = $dbDirections->GetNextElement())
	$arDirections[] = $obDirections->GetFields();
if ($_GET['directions'] == 'null')
    $DirectionsID ='';
else
    $DirectionsID = explode(',', $_GET['directions']);
if($DirectionsID){
    $GLOBALS["arPropFilter"] = Array(
        "PROPERTY_DIRECTIONS_FILTER" => $DirectionsID,
    );
}
$ClinicCounter = 0;
$ClinicSelect = array(
    "NAME",
    "DETAIL_PICTURE",
    "DETAIL_PAGE_URL",
    );
if ($engTest == "RU"){
    $dbClinics = CIBlockElement::GetList(array(),array('IBLOCK_ID' => '7',"PROPERTY_DIRECTIONS_FILTER" => $DirectionsID),false,false,$ClinicSelect);
}
else{
   $dbClinics = CIBlockElement::GetList(array(),array('IBLOCK_ID' => '8',"PROPERTY_DIRECTIONS_FILTER" => $DirectionsID),false,false,$ClinicSelect); 
}

while ($obClinics = $dbClinics->GetNextElement()) {
    $arClinics[$ClinicCounter] = $obClinics->GetFields();
    $arClinics[$ClinicCounter]["PROPERTIES"] = $obClinics->GetProperties();
    $ClinicCounter++;
}

?>
<div class="filter bg-full-width">
    <div class="content">
        <div class="service">
            <select data-placeholder="<?echo GetMessage("NEWS_DIRECT")?>" class="chosen-select select-list" multiple='true' tabindex="4">
                <option value=""></option>
				<?$CountOption=0;
				foreach ($arDirections as $Directions):?>
                	<?$CountOption++;?>
                    <?if(in_array($Directions["ID"], $DirectionsID)):?>
                	    <option selected data-count="<?=$CountOption?>" value="<?=$Directions["ID"]?>"><?=$Directions["NAME"]?></option>
				    <?else:?>
                        <option data-count="<?=$CountOption?>" value="<?=$Directions["ID"]?>"><?=$Directions["NAME"]?></option>
                    <?endif;?>                    
                <?endforeach;?>
            </select>
        </div><!--services-->		
        <button class="filter-button"><?echo GetMessage("NEWS_SUBMIT")?></button>
        <div class="search">
        <?$APPLICATION->IncludeComponent("bitrix:search.form", "search_clinics", Array(
	"PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
		"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
	),
	false
);?>
            <!-- <span class="search-btn">
                <i class="icon ico-search pos-center"></i>
            </span> -->

            <!-- <input type="text" name="search" placeholder="Поиск"/> -->

        </div><!--search-->
        <div class="clear"></div>

    </div><!--content-->

</div><!--filter-->
<h1 class="page-title content">
    <strong><?echo GetMessage("NEWS_CLINICS")?></strong>
    <?if ($engTest == "RU"){?>
        <span class="count"><?echo GetMessage("SEARCH_LABEL")?> <?=count($arClinics);?> <?=getNumEnding(count($arClinics),$arClinic);?></span>
    <?}
    else{?>
        <span class="count"><?=count($arClinics);?> clinics</span>
    <?}?>
</h1>
<div class="main content">
    <div class="sort spacer">
        <!-- <div class="select-list">
                <div class="section">
                    <select>
                        <option>Сортировка</option>
                        <option>Сортировка1</option>
                        <option>Сортировка2</option>
                        <option>Сортировка3</option>
                        <option>Сортировка4</option>
                    </select>
                </div>

        </div>

        <ul class="view-type">
            <li class="i1"><span></span>Список</li>
            <li class="i2 active"><span></span>Плитка</li>
            <li class="i3"><span></span>Карта</li>
        </ul> -->
        <div class="number-wrap" style="  width: 370px;">
            <p style="float: left;"><?echo GetMessage("NEWS_SHOW_COUNT")?></p>
            <ul class="view-number">
                <li class="active" value="10">10</li>
                <li value="20">20</li>
                <li value="30">30</li>
                <li value="40">40</li>
                <li value="10000"><?echo GetMessage("NEWS_ALL")?></li>
            </ul><!--view-number-->
        </div>
    </div><!--sort-->

    <div class="clinics-listing">
        <?foreach ($arClinics as $arItem):?>
            <?if ($arItem["PROPERTIES"]["VIP_CLINIC"]["VALUE"] == "Y"):?>
                <div class="item-vip">
                    <?$image = CFile::GetPath($arItem["DETAIL_PICTURE"]);?>
                    <img src="<?=$image?>" style="width: 1170px; height: 350px;" alt="<?=$arItem["DETAIL_PICTURE"]?>"/>
                    <div class="item-content box">
                        <p class="item-name"><?=$arItem["NAME"];?></p>
                        <div class="item-raring">
                            <?
                                $StarRating = round(($arItem["PROPERTIES"]["DEGREE_OF_SERVICE"]["VALUE"] +
                                                     $arItem["PROPERTIES"]["PRICE_QUALITY"]["VALUE"] +
                                                     $arItem["PROPERTIES"]["OPERATIV"]["VALUE"] +
                                                     $arItem["PROPERTIES"]["USER_RATING"]["VALUE"])/4);
                                $NotStarRating = 5-$StarRating;
                            ?>
                            <?for ($RaringCount=0; $RaringCount < $StarRating; $RaringCount++):?>
                                <i class="icon ico-star-wht-slt"></i>
                            <?endfor;?>
                            <?for ($RaringCount=0; $RaringCount < $NotStarRating; $RaringCount++):?>
                                <i class="icon ico-star-wht"></i>
                            <?endfor;?>
                        </div><!--raring-->

                        <p class="item-service">
                            <i class="icon ico-service-white"></i>
                            <?
                            $direction = CIBlockElement::GetByID($arItem["PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"]["0"]);
                            $direction = $direction->GetNext()
                            ?>
                            <span class="text"><?=$direction["NAME"]?></span>
                        </p>

                        <p class="item-city">
                            <i class="icon ico-mark-white"></i>
                            <span class="text">г. <?=$arItem["PROPERTIES"]["TOWN"]["VALUE"]?></span>
                        </p>

                        <p class="item-price">
                            <i class="icon ico-price-white"></i>
                            <span class="text">от <?=$arItem["PROPERTIES"]["COST_OF_SERVICES"]["VALUE"]?> руб.</span>
                        </p>

                        <p class="item-desc">
                            <?
                            $directionCountmax = count($arItem["PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"]);
                            $directionCount = 0;
                            ?>
                            <?foreach ($arItem["PROPERTIES"]["DIRECTIONS_FILTER"]["VALUE"] as $direction) {
                            $directionName = CIBlockElement::GetByID($direction);
                            $directionName = $directionName->GetNext();
                                if ($directionCountmax-1 == $directionCount)
                                    echo $directionName["NAME"];
                                else {
                                    echo $directionName["NAME"].", ";
                                    $directionCount++;
                                }
                            }?>
                        </p>

                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn"><?echo GetMessage("NEWS_READ_MORE")?></a>

                    </div><!--item-content--> 
                </div><!--item-vip-->
                <span class="howto-get-vip"><?echo GetMessage("NEWS_VIP_PLACE")?><a href="#"><?echo GetMessage("NEWS_VIP_HOW")?></a></span>
            <?break;?>
            <?endif;?>
        <?endforeach;?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$clinics_count,//$arParams["NEWS_COUNT"],
		"SORT_BY1"	=>	$arParams["SORT_BY1"],
		"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	"arPropFilter",
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
	),
	$component
);?>
