<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<form id="search-clinics" action="<?=$arResult["FORM_ACTION"]?>">
	<span class="search-btn">
        <i class="icon ico-search pos-center"></i>
    </span>
	<input type="text" name="q" value="" size="15" maxlength="50" placeholder="<?echo getMessage("CLINICS_SEARCH")?>" />
</form>
<script type="text/javascript">
	$('.search-btn').on('click',function(){
		$('#search-clinics').submit();
	});
</script>
