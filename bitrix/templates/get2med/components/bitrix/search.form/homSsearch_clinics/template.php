<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>



<form id="search-clinics" action="<?=$arResult["FORM_ACTION"]?>">
<div class="filter bg-full-width search-box">
    <div class="content">
        <div class="hdr"><?echo GetMessage("HOME_CANT_FIND")?></div>
        <div class="search">
            <span class="search-btn">
                <i class="icon ico-search pos-center"></i>
                <b><?echo GetMessage("HOME_SEARCH")?></b>
            </span>
            <input type="text" name="q" placeholder="<?echo GetMessage("HOME_DIRECT_SEARCH")?>">
            <div class="example"><?echo GetMessage("HOME_EXAMPLE")?></div>

        </div><!--search-->
    </div><!--content-->
</div>
</form>
<script type="text/javascript">
	$('.search-btn').on('click',function(){
		$('#search-clinics').submit();
	});
</script>