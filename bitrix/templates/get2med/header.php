<?
$curpage = $APPLICATION->GetCurPage();
$page_code = explode('/', $curpage);
?>
<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead()?>
    
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/reset.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/chosen.css");?>
    <?$APPLICATION->AddHeadScript('http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/bxslider/jquery.bxslider.min.js');?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/bxslider/jquery.bxslider.css");?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.selectbox.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/sliders.js');?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/fancy/jquery.fancybox.css?v=2.1.5');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/fancy/jquery.fancybox.pack.js?v=2.1.5');?>
    <?$APPLICATION->AddHeadScript('https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=de');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js');?>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox({
                maxWidth    : 800,
                maxHeight   : 600,
                fitToView   : false,
                width       : '80%',
                height      : 'auto',      
            });
        });
</script>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
	<?if($APPLICATION->GetCurPage() == '/' || $APPLICATION->GetCurPage() == '/en/'):?>
	<div class="top-bg">
	<?endif;?>
        <div class="nav bg-full-width">
            <div class="content">
                <a class="logo" href="<?=SITE_DIR?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="Логотип"/></a>
        		<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"top_menu",
					Array(
						"ROOT_MENU_TYPE" => "top",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(""),
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "top",
						"USE_EXT" => "N",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N"
					)
				);?>
        
                <div class="language">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.site.selector",
                        "",
                        Array(
                            "SITE_LIST" => array("s1", "en"),
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600"
                        )
                    );?>
                    <!-- <span class="ru active">Рус</span>
                    <span class="en">Eng</span> -->
        
                </div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "PATH" => "/include/header_right_phone.php"
                    )
                );?>
	            <a class="authorization" href="tel:+88003331492">8 (800) 333-14-92</a>
                <!-- <a class="authorization" href="#">Войти</a>
                <button class="registration">Регистрация</button> -->
        
            </div><!--content-->
    
        </div>
		<?if(
		$APPLICATION->GetCurPage() != '/' AND $APPLICATION->GetCurPage() != '/en/' AND
        ($page_code['1']  != 'clinics' AND $page_code['2'] != 'clinics')
		){?>
		<div class="content">
			<h1 class="page-title"><?$APPLICATION->ShowTitle(false)?></h1>
			<div class="main">
		<?}?>
