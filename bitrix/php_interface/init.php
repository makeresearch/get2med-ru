<?
/*
You can place here your functions and event handlers

AddEventHandler("module", "EventName", "FunctionName");
function FunctionName(params)
{
	//code
}
*/

AddEventHandler("main", "OnEpilog", "Redirect404");
function Redirect404() {
	if( 
	 !defined('ADMIN_SECTION') &&	
	 defined("ERROR_404") &&	
	 defined("PATH_TO_404") &&	
	 file_exists($_SERVER["DOCUMENT_ROOT"].PATH_TO_404) 
	 ) {
		//LocalRedirect("/404.php", "404 Not Found");
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		CHTTP::SetStatus("404 Not Found");
		include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header.php");
		include($_SERVER["DOCUMENT_ROOT"].PATH_TO_404);
		include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer.php");
	}
}

/**
 * Функция для обработки изображений
 * @param	string $template - Шаблон обработки изображений создается в /bitrix/admin/webdebug_image_profiles.php?lang=ru
 * @param	string $src - SRC обрабатываемой картинки
 * @return array Массив с различной информацией о новой картинке
 */
function webImage($template, $src){
	$arImg = $GLOBALS['APPLICATION']->IncludeComponent(
	'webdebug:image',
	'.default',
	Array(
		'PROFILE' => $template,
		'RETURN' => 'ARRAY',
		'IMAGE' => $src,
		'CACHE_IMAGE' => 'Y',
		'DISPLAY_ERRORS' => 'N'
	),
	$component,
	array('HIDE_ICONS'=>'Y')
	);
	
	$fullsrc = $_SERVER['DOCUMENT_ROOT'] . $src;
	if(((!isset($arImg['SIZE']) OR $arImg['SIZE'] == 0)) AND file_exists($fullsrc)) {
	$ext = explode('.', $arImg['FILE_NAME']);
	$ext = trim(strtolower($ext[count($ext)-1])); 
	if($ext == 'gif') {
		$onlypath = explode('/', $fullsrc);
		unset($onlypath[count($onlypath)-1]);
		$onlypath = implode('/', $onlypath);
		if(!is_dir($onlypath)) {
		mkdir($onlypath, 755, true);
		}
		if(class_exists('Imagick')){
		$fullsrc_jpg = str_replace('.gif', '.jpg', $fullsrc);
		if(!file_exists($fullsrc_jpg)) {
			$image = new Imagick($fullsrc);
			$image->setImageFormat('jpeg');
			$image->setImageCompressionQuality(100);
			$image->writeimage($fullsrc_jpg);
		}
		if(file_exists($fullsrc_jpg)) {
			$arImg = $GLOBALS['APPLICATION']->IncludeComponent(
			'webdebug:image',
			'.default',
			Array(
				'PROFILE' => $template,
				'RETURN' => 'ARRAY',
				'IMAGE' => str_replace($_SERVER['DOCUMENT_ROOT'], '', $fullsrc_jpg),
				'CACHE_IMAGE' => 'Y',
				'DISPLAY_ERRORS' => 'N'
			),
			$component,
			array('HIDE_ICONS'=>'Y')
			);
		}
		}
	}
	}
	return $arImg;
}
?>